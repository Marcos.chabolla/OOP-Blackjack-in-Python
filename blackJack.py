import random

class Card():
	def __init__(self, cardRank, cardSuit, cardValue):
		self.rank = str(cardRank)
		self.suit = cardSuit
		self.value = cardValue;

	def __str__(self):
		return self.rank + " " +self.suit+ " " + str(self.value) + " | "

class Deck():

	def __init__(self):
		self.hardCopy = []
		self.deck = []
		for x in range(1, 14):
			if x == 1:
				self.deck.append(Card("Ace", "Club", 11))
				self.deck.append(Card("Ace", "Spade", 11))
				self.deck.append(Card("Ace", "Diamond", 11))
				self.deck.append(Card("Ace", "Heart", 11))
				continue
			if x == 11:
				self.deck.append(Card("Jack", "Club", 10))
				self.deck.append(Card("Jack", "Spade", 10))
				self.deck.append(Card("Jack", "Diamond", 10))
				self.deck.append(Card("Jack", "Heart", 10))
				continue
			if x == 12:
				self.deck.append(Card("Queen", "Club", 10))
				self.deck.append(Card("Queen", "Spade", 10))
				self.deck.append(Card("Queen", "Diamond", 10))
				self.deck.append(Card("Queen", "Heart", 10))
				continue
			if x == 13:
				self.deck.append(Card("King", "Club", 10))
				self.deck.append(Card("King", "Spade", 10))
				self.deck.append(Card("King", "Diamond", 10))
				self.deck.append(Card("King", "Heart", 10))
				continue

			self.deck.append(Card(x, "Club", x))
			self.deck.append(Card(x, "Spade", x))
			self.deck.append(Card(x, "Diamond", x))
			self.deck.append(Card(x, "Heart", x))
			# print "Added card" , x
		self.hardCopy = self.deck	
		random.shuffle(self.deck)


	def draw(self):
		if len(self.deck) < 10:
			self.deck = random.shuffle(self.hardCopy)
		newCard = self.deck.pop()
		return newCard

	def shuffle():
		random.shuffle(self.deck)

class Player():
	def __init__(self):
		self.hand = []
		self.bust = 0
		self.totalMoney = 100

	def handValue(self):
		handVal = 0
		bigAceCount = 0

		for card in self.hand:
			if card.value == 11:
				bigAceCount += 1

			handVal += card.value

		while handVal > 21 and bigAceCount > 0:
			for card in self.hand:
				if card.value == 11:
					handVal -= 10
					bigAceCount -= 1
					break

		if handVal > 21:
			self.bust = 1

		return handVal

	def printHand(self):
		for card in self.hand:
			print(card)

	def isBust(self):
		return self.bust

	def isBlackJack(self):
		if self.handValue() == 21:
			return 1
		else:
			return 0

	def addCard(self, card):
		self.hand.append(card)

	def bet(self):
		self.totalMoney -= 10

	def winMoney(self, winnings):
		self.totalMoney += winnings

	def blackJackHonorableSedoku(self):
		self.bust = 1;

	def enter(self):
		self.bust = 0

	def newHand(self):
		self.hand = []

	def printMoney(self):
		print("This player has :", self.totalMoney)


class Game():
	def __init__(self):
		self.playerOne = Player()
		self.playerTwo = Player()
		self.playerList = (self.playerOne, self.playerTwo)
		self.dealer = Player()
		self.deckObj = Deck()

	def deal(self):
		for player in self.playerList:
			player.enter()
			player.newHand()

			cardOne = self.deckObj.draw()
			cardTwo = self.deckObj.draw()

			player.addCard(cardOne)
			player.addCard(cardTwo)

		self.dealer.newHand()
		dealerCardOne = self.deckObj.draw()
		dealerCardTwo = self.deckObj.draw()

		self.dealer.addCard(dealerCardOne)
		self.dealer.addCard(dealerCardTwo)

		#This is bad OOP Design but I didnt want to redo the formatting for print statements
		print("_________________")
		print("Dealer hand: ", self.dealer.hand[0].value, "+ X")
		print("Player One Hand: ", self.playerOne.hand[0].value, " + ", self.playerOne.hand[1].value, " = ", self.playerOne.hand[0].value + self.playerOne.hand[1].value)
		print("Player Two Hand: ", self.playerTwo.hand[0].value, " + ", self.playerTwo.hand[1].value, " = ", self.playerTwo.hand[0].value + self.playerTwo.hand[1].value)
		print("_________________")

def main():
	blackJack = Game()

	while(1):
		blackJack.deal() #Deals all cards to players and dealer
		playerNum = 1
		playerIndex = 0

		for player in blackJack.playerList:

			######## PLAYER TURNS #########

			if player.handValue() == 21:
				print("Player ", playerNum, "got natural BlackJack! +$30")
				playerNum += 1
				playerIndex += 1
				continue

			print("_________________")
			print("Player ", playerNum, "'s turn!")

			player.printMoney()
			player.bet()
			print("Player ", playerNum, "bets $10")
			player.printMoney()
			print("Player ", playerNum, "'s hand value:", player.handValue())
			print("_________________")

			moveOne = input("Hit or Hold?: ")

			while moveOne != "Hold":
				player.addCard(blackJack.deckObj.draw())
				player.printHand()
				print(player.handValue())
				print("_________________")
				if player.isBust():
					print("You busted: ", player.handValue())
					break
				elif player.isBlackJack():
					print("21! Your money's comin'")
					break
				moveOne = input("Hit or Hold?: ")

			playerNum += 1
			playerIndex += 1


		######## DEALER TURN ########

		print("_________________")
		print("Dealers hand is: ", blackJack.dealer.handValue())

		while blackJack.dealer.handValue() < 17:
			blackJack.dealer.addCard(blackJack.deckObj.draw())
			print("The dealer hits! New hand value: ", blackJack.dealer.handValue())

		######## RESOLVE ROUND #######
		if blackJack.dealer.isBust():
			print("Dealer busted!", blackJack.dealer.handValue())
			print("All players remaining win their bets!")
			playerCount = 1
			for player in blackJack.playerList:
				if player.isBust():
					playerCount += 1
					continue
				else:
					player.winMoney(20)
					print("Player ", playerCount, "wins the bet since the dealer busted!: +$20")
					player.printMoney()
					print("_________________")
					playerCount += 1

		elif blackJack.dealer.isBlackJack():
			print("Dealer has blackJack!", blackJack.dealer.handValue())
			print("_________________")
			playerCount = 1
			for player in blackJack.playerList:
				if player.handValue() == blackJack.dealer.handValue():
					print("Player ", playerCount, "also has Blackjack! He gets his bet money back: +$10")
					player.winMoney(10)
					player.printMoney()
					print("_________________")
				playerCount += 1	

		#Resolve rest of bets
		else:
			print("_________________")
			print("Round Resolve:")
			playerCount = 1
			for player in blackJack.playerList:
				if not player.isBust():
					print("Player", playerCount, "didnt bust")
					if blackJack.dealer.handValue() < player.handValue():
						print("This player also had a higher hand than the dealer", playerCount)
						print("Player ", playerCount, "won $20!")
						player.winMoney(20)
						player.printMoney()
					elif blackJack.dealer.handValue() == player.handValue():
						player.winMoney(10)
						print("Player ", playerCount, "tied the dealer: +$10!")
					else:
						print("But their hand wasnt higher than the dealer! +$0")
				else:
					print("Player ", playerCount, "Busted! +$0")


				print("_________________")
				playerCount += 1

		print("The round has resolved!")
		print("Starting a new round...")
		print("_________________")

if __name__ == '__main__':
	main()
